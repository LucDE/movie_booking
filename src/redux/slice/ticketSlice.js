const { createSlice } = require("@reduxjs/toolkit");

let initialState = {
  listTicket: [],
};

const ticketSlice = createSlice({
  name: "ticketSlice",
  initialState,
  reducers: {
    bookTicket: (state, action) => {
      state.listTicket.push(action.payload);
    },
  },
});
export const { bookTicket } = ticketSlice.actions;

export default ticketSlice.reducer;
