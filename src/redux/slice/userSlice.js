const { createSlice } = require("@reduxjs/toolkit");

let initialState = {
  isLogin: true,
  userInfor: null,
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = action.payload;
    },
  },
});

export const { setUserInfor } = userSlice.actions;

export default userSlice.reducer;
