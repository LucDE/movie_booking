const { createSlice } = require("@reduxjs/toolkit");

let initialState = {
  listChoosenChair: [],
};

const chairSlice = createSlice({
  name: "chairSlice",
  initialState,
  reducers: {
    chooseChair: (state, action) => {
      let checkIndex = state.listChoosenChair.findIndex((item) => {
        return item.maGhe === action.payload.maGhe;
      });
      if (checkIndex !== -1) {
        state.listChoosenChair.splice(checkIndex, 1);
      } else {
        state.listChoosenChair.push(action.payload);
      }
    },
    resetChair: (state, action) => {
      state.listChoosenChair = [];
    },
  },
});
export const { chooseChair, resetChair } = chairSlice.actions;

export default chairSlice.reducer;
