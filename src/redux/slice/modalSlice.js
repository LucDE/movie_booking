import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  modalTrailer: {
    isOpen: false,
    videoId: "",
  },
};

const modalSlice = createSlice({
  name: "modalSlice",
  initialState,
  reducers: {
    openModalTrailer: (state, action) => {
      state.modalTrailer.isOpen = true;
      state.modalTrailer.videoId = action.payload;
    },
    closeModalTrailer: (state, action) => {
      state.modalTrailer.isOpen = false;
    },
  },
});

export const { openModalTrailer, closeModalTrailer } = modalSlice.actions;

export default modalSlice.reducer;
