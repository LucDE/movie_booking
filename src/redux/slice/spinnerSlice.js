import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  count: 0,
};

const spinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    turnOnLoading: (state, action) => {
      state.count++;
      state.isLoading = true;
    },
    turnOffLoading: (state, action) => {
      state.count--;
      if (state.count === 0) {
        state.isLoading = false;
      }
    },
  },
});

export const { turnOnLoading, turnOffLoading } = spinnerSlice.actions;

export default spinnerSlice.reducer;
