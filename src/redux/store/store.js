import { configureStore } from "@reduxjs/toolkit";
import userSlice from "../slice/userSlice";
import chairSlice from "../slice/chairSlice";
import modalSlice from "../slice/modalSlice";
import spinnerSlice from "../slice/spinnerSlice";

export const store = configureStore({
  reducer: {
    userSlice,
    chairSlice,
    modalSlice,
    spinnerSlice,
  },
});
