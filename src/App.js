import "../node_modules/react-modal-video/css/modal-video.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import ModalTrailer from "./components/ModalTrailer";
import { routes } from "./Routes/routes";
import { nanoid } from "nanoid";
import Spinner from "./components/Spinner";

export default function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Navbar />
        <Routes>
          {routes.map(({ path, component }) => {
            return <Route key={nanoid()} path={path} element={component} />;
          })}
        </Routes>
        <ModalTrailer />
      </BrowserRouter>
    </div>
  );
}
