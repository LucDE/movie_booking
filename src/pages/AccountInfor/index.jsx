import { message, Tabs } from "antd";
import axios from "axios";
import moment from "moment";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { userInforLocal } from "services/local.service";
import { BASE_URL, TOKEN_CYBERSOFT } from "services/url.config";
import { userServ } from "services/userService";
import { Button, Form, Input } from "antd";
import { setUserInfor } from "redux/slice/userSlice";
import { useDispatch } from "react-redux";

const styleBookingPage = {
  backgroundImage:
    "linear-gradient(to top,#000,transparent 70%),url(https://www.maxpixel.net/static/photo/1x/Red-Curtain-Cinema-Background-Theater-Stripes-551797.jpg",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
};

export default function AccountInfor() {
  let [accountInfor, setAccountInfor] = useState(null);

  let dispatch = useDispatch();

  let fetchUserInfor = async () => {
    axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
      method: "POST",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + userInforLocal.get()?.accessToken,
      },
    })
      .then((res) => {
        setAccountInfor(res.data.content);
        dispatch(setUserInfor(res.data.content));
      })
      .catch((err) => {});
  };

  useEffect(() => {
    fetchUserInfor();
  }, []);

  const onFinish = (values) => {
    let { taiKhoan, matKhau, hoTen, email, soDT } = values;

    let newData = {
      taiKhoan,
      matKhau,
      email,
      soDT,
      maNhom: accountInfor.maNhom,
      maLoaiNguoiDung: accountInfor.maLoaiNguoiDung,
      hoTen,
    };

    userServ
      .updateAccountInfor(newData)
      .then((res) => {
        message.success("Cập nhật thành công!");
        fetchUserInfor();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  let renderInfor = () => {
    if (accountInfor !== null) {
      return (
        <Form
          initialValues={{
            taiKhoan: accountInfor?.taiKhoan,
            matKhau: accountInfor?.matKhau,
            moTa: accountInfor?.moTa,
            hoTen: accountInfor?.hoTen,
            email: accountInfor?.email,
            soDT: accountInfor?.soDT,
          }}
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item
            name="taiKhoan"
            label={<span className="text-white">Tài khoản:</span>}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="matKhau"
            label={<span className="text-white">Mật khẩu:</span>}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="hoTen"
            label={<span className="text-white">Họ tên:</span>}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="email"
            label={<span className="text-white">Email:</span>}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="soDT"
            label={<span className="text-white">Số điện thoại:</span>}
          >
            <Input />
          </Form.Item>
          <Form.Item label={<span className="text-white">Thao tác:</span>}>
            <Button htmlType="submit">
              <span className="text-white hover:text-sky-500">
                Cập nhật thông tin
              </span>
            </Button>
          </Form.Item>
        </Form>
      );
    }
  };

  let renderTikets = () => {
    if (accountInfor !== null) {
      return accountInfor.thongTinDatVe.map((ticket) => {
        return (
          <div
            key={nanoid()}
            className="df:border-2 df:border-white df:p-2 flex flex-col items-start space-y-4"
          >
            <span className="text-amber-400">
              Ngày giờ đặt:{" "}
              {" " + moment(ticket.ngayDat).format("DD-MM-YYYY - HH:MM")}
            </span>
            <span className="text-amber-400 text-xl">{ticket.tenPhim}</span>
            <span className="text-amber-400">
              Giá vé: {ticket.giaVe + " VNĐ"}
            </span>
            <span className="text-amber-400">
              Thời lượng phim: {ticket.thoiLuongPhim + " phút"}
            </span>
            {ticket.danhSachGhe.map((ghe, index) => {
              return (
                <div key={nanoid()} className="space-x-4">
                  <span className="text-white">Vé {index + 1}:</span>
                  <span className="text-amber-400">{ghe.tenHeThongRap}</span>
                  <span className="text-amber-400">{ghe.tenRap}</span>
                  <span className="text-amber-400">
                    Số ghế:{" " + ghe.tenGhe}
                  </span>
                </div>
              );
            })}
          </div>
        );
      });
    }
  };

  return (
    <div
      style={styleBookingPage}
      className="w-screen h-screen text-white font-bold space-y-8 df:px-10 df:py-20 sm:p-32 overflow-y-scroll"
    >
      <div>
        <h1 className="df:text-2xl sm:text-4xl font-bold text-center text-white">
          Thông tin tài khoản
        </h1>
      </div>
      <Tabs tabPosition="top">
        <Tabs.TabPane
          tab={<p className="text-white font-bold">Thông tin cá nhân</p>}
          key="item-tab-1"
        >
          {renderInfor()}
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={<p className="text-white font-bold">Lịch sử đặt vé</p>}
          key="item-tab-2"
        >
          <div className="grid df:grid-cols-1 df:gap-3 md:grid-cols-2 md:gap-6 xl:grid-cols-3 xl:gap-10  ">
            {renderTikets()}
          </div>
        </Tabs.TabPane>
      </Tabs>
    </div>
  );
}
