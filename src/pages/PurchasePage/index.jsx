import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { resetChair } from "redux/slice/chairSlice";
import { purchaseServ } from "services/purchaseService";
import ChairList from "./ChairList";
import MovieInfor from "./MovieInfor";

const pageStyle = {
  backgroundImage:
    "linear-gradient(to right,rgba(0, 0, 0, 0.9) 150px,rgba(0, 0, 0, 0.6) 100%),url(https://media.istockphoto.com/photos/empty-red-armchairs-of-a-theater-ready-for-a-show-picture-id1295114854?b=1&k=20&m=1295114854&s=170667a&w=0&h=W9ZbN674554Jsamxo5AfoO3DrSm_7qYS1EnANgusi9o=)",
  backgroundSize: "cover",
};

export default function PurchasePage() {
  let [chairsList, setChairsList] = useState([]);
  let [movieDetail, setMovieDetail] = useState({});

  let dispatch = useDispatch();

  let params = useParams();
  let { id } = params;

  let fetchData = async () => {
    try {
      let res = await purchaseServ.getTicketRoomServ(id);

      if (res) {
        let newchairsList = res.data.content.danhSachGhe;
        let newmovieDetail = res.data.content.thongTinPhim;
        setChairsList(newchairsList);
        setMovieDetail(newmovieDetail);
      }
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  useEffect(() => {
    fetchData();
    dispatch(resetChair());
  }, [dispatch]);

  return (
    <div
      style={pageStyle}
      className="purchase_page w-screen h-screen df:pt-20 xl:grid xl:grid-cols-10 sm:pt-32 sm:p-5 overflow-y-scroll"
    >
      <ChairList chairsList={chairsList} />
      <MovieInfor movieDetail={movieDetail} />
    </div>
  );
}
