import React from "react";

import { useDispatch } from "react-redux";
import { devideArray } from "services/devideArray";
import { nanoid } from "nanoid";
import { chooseChair } from "redux/slice/chairSlice";
import "./index.scss";

export default function ChairList({ chairsList }) {
  let dispatch = useDispatch();

  const handleChooseChair = (e, ghe) => {
    let buttonChoosing = document.getElementById(e.currentTarget.id);
    let buttonChoosingClassName = buttonChoosing.className;
    let checkChoosen = buttonChoosingClassName.search("bg-green-500");
    if (checkChoosen === -1) {
      buttonChoosing.className =
        buttonChoosingClassName.search("vip_chair") === 0
          ? "vip_chair bg-green-500 hover:bg-white rounded text-black"
          : "normal_chair bg-green-500 hover:bg-white rounded text-black";
    }
    if (checkChoosen !== -1) {
      buttonChoosing.className =
        buttonChoosingClassName.search("vip_chair") === 0
          ? "vip_chair bg-amber-500 hover:bg-white rounded text-black"
          : "normal_chair bg-gray-400 hover:bg-white rounded text-black";
    }
    let { maGhe, giaVe, tenGhe } = ghe;
    let newGhe = { maGhe: Number(maGhe), giaVe: Number(giaVe), tenGhe };
    dispatch(chooseChair(newGhe));
  };
  let renderChairList = () => {
    if (chairsList !== []) {
      let devideChairsList = devideArray(chairsList);

      return devideChairsList.map((danhSach) => {
        return (
          <tr key={nanoid()}>
            {danhSach.map((ghe) => {
              return (
                <td key={ghe.maGhe}>
                  <button
                    disabled={ghe.daDat ? true : false}
                    className={
                      ghe.loaiGhe === "Vip"
                        ? "vip_chair bg-amber-500 hover:bg-white rounded text-black"
                        : "normal_chair bg-gray-400 hover:bg-white rounded text-black"
                    }
                    id={`btn_choose_chair_${ghe.tenGhe}`}
                    style={
                      ghe.daDat ? { backgroundColor: "rgb(75 85 99)" } : {}
                    }
                    onClick={(e) => {
                      handleChooseChair(e, ghe);
                    }}
                  >
                    {ghe.daDat ? (
                      <span className="flex justify-center items-center">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="df:w-2 df:h-2 sm:w-4 sm:h-4"
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="#000000"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        >
                          <line x1={18} y1={6} x2={6} y2={18} />
                          <line x1={6} y1={6} x2={18} y2={18} />
                        </svg>
                      </span>
                    ) : (
                      <span className="flex justify-center items-center df:text-small sm:text-base">
                        {ghe.tenGhe}
                      </span>
                    )}
                  </button>
                </td>
              );
            })}
          </tr>
        );
      });
    }
  };
  return (
    <div className="choose__chair__part flex flex-col items-center lg:col-span-7 df:px-5 sm:px-0">
      <span className="df:text-xs sm:text-base text-white">Màn hình</span>
      <div className="df:w-4/5 df:h-1 sm:h-2 lg:w-800  mb-8" id="screen" />
      <table className="chair">
        <tbody>{renderChairList()}</tbody>
      </table>
      <div className="flex space-x-5 py-5">
        <div className="flex items-center space-x-2">
          <button
            disabled
            className="flex justify-center items-center df:w-3 df:h-3 sm:w-5 sm:h-5 bg-gray-600 "
          >
            <span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="df:w-2 df:h-2 sm:w-4 sm:h-4"
                viewBox="0 0 24 24"
                fill="none"
                stroke="#000000"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <line x1={18} y1={6} x2={6} y2={18} />
                <line x1={6} y1={6} x2={18} y2={18} />
              </svg>
            </span>
          </button>
          <p className="df:text-sm sm:text-base text-white">Ghế đã đặt</p>
        </div>
        <div className="flex items-center space-x-2">
          <div className="green df:w-3 df:h-3 sm:w-5 sm:h-5 bg-amber-500" />
          <p className="df:text-sm sm:text-base text-white">Vip</p>
        </div>
        <div className="flex items-center  space-x-2">
          <div className="white df:w-3 df:h-3 sm:w-5 sm:h-5 bg-gray-400" />
          <p className="df:text-sm sm:text-base text-white">Thường</p>
        </div>
      </div>
    </div>
  );
}
