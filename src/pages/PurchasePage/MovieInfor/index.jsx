import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";
import { ticketServ } from "services/ticketService";
import { message } from "antd";

export default function TicketInfor({ movieDetail }) {
  let listChoosenChair = useSelector(
    (state) => state.chairSlice.listChoosenChair
  );

  let userInfor = useSelector((state) => state.userSlice.userInfor);
  let navigate = useNavigate();
  let handleBookTicket = () => {
    if (userInfor === null) {
      swal({
        icon: "error",
        title: "Bạn chưa đăng nhập",
        text: "Bạn có muốn đăng nhập không?",
        buttons: {
          cancel: "Không",
          confirm: "Đồng ý",
        },
      }).then((value) => {
        if (value) {
          navigate("/login");
        }
      });
      return;
    }

    if (listChoosenChair.length === 0) {
      swal({
        icon: "error",
        title: "Bạn chưa chọn ghế",
      });
      return;
    }
    let newTicket = {
      maLichChieu: Number(movieDetail.maLichChieu),
      danhSachVe: listChoosenChair,
    };

    ticketServ
      .postTicket(newTicket)
      .then((res) => {
        swal({
          icon: "success",
          title: "Đặt ghế thành công!",
          text: "Kiểm tra trong lịch sử đặt vé",
        });
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };
  let renderThongTinVe = () => {
    if (movieDetail) {
      return (
        <div className="ticket_info_container text-slate-200 df:space-y-4 df:px-4 df:py-4 sm:space-y-4 sm:p-5 lg:w-4/6 xl:w-full mx-auto">
          <div>
            <p className="df:text-2xl text-center uppercase font-bold text-amber-500 border df:border-amber-500 sm:border-transparent">
              thông tin vé
            </p>
          </div>
          <div>
            <p className="df:text-xl sm:text-2xl text-center text-red-500">
              {listChoosenChair
                .map((item) => {
                  return item.giaVe;
                })
                .reduce((a, b) => {
                  return a + b;
                }, 0)
                .toLocaleString() + " VNĐ"}
            </p>
          </div>
          <div className="flex justify-between df:text-base sm:text-lg">
            <span>Cụm rạp:</span>
            <span className="text-red-500">{movieDetail.tenCumRap}</span>
          </div>
          <div className="flex justify-between df:text-base sm:text-lg">
            <span>Địa chỉ:</span>
            <span className="text-red-500 text-right">
              {movieDetail.diaChi}
            </span>
          </div>
          <div className="flex justify-between df:text-base sm:text-lg">
            <span>Rạp:</span>
            <span className="text-red-500">{movieDetail.tenRap}</span>
          </div>
          <div className="flex justify-between df:text-base sm:text-lg">
            <div>
              <span>Ngày giờ chiếu:</span>
            </div>
            <div className="text-red-500">
              <span>{movieDetail.ngayChieu}</span> <span> - </span>
              <span>{movieDetail.gioChieu}</span>
            </div>
          </div>
          <div className="flex justify-between df:text-base sm:text-lg">
            <span>Tên phim:</span>
            <span className="text-red-500">{movieDetail.tenPhim}</span>
          </div>
          <div className="flex justify-between df:text-base sm:text-lg">
            <span>Chọn:</span>
            <span className="text-red-500">
              {listChoosenChair.map((item) => {
                return "Ghế " + item.tenGhe + ", ";
              })}
            </span>
          </div>
          <div className="flex justify-center">
            <button
              className="df:w-full md:w-2/3 lg:w-1/3 xl:w-full bg-amber-500 hover:bg-red-500 p-3 rounded"
              onClick={() => {
                handleBookTicket();
              }}
            >
              ĐẶT VÉ
            </button>
          </div>
        </div>
      );
    }
  };
  return <div className="ticket_info xl:col-span-3">{renderThongTinVe()}</div>;
}
