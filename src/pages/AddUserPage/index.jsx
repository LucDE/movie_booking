import React from "react";
import { userServ } from "services/userService";
import { useNavigate } from "react-router-dom";
import { Button, Form, Input, message, Select } from "antd";
const { Option } = Select;

export default function AddUserPage() {
  let navigate = useNavigate();

  const onFinish = (values) => {
    let { taiKhoan, matKhau, maLoaiNguoiDung, hoTen, email, soDT } = values;

    let newData = {
      taiKhoan,
      matKhau,
      email,
      soDT,
      maNhom: "GP01",
      maLoaiNguoiDung,
      hoTen,
    };
    userServ
      .addUserAccount(newData)
      .then((res) => {
        message.success("Thêm người dùng thành công!");
        setTimeout(() => {
          navigate("/user-management");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  return (
    <div className="container  px-10 py-32">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl text-center font-bold mb-5">ADD USER</h1>
        <Form
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item name="taiKhoan" label="Tài khoản">
            <Input />
          </Form.Item>
          <Form.Item name="matKhau" label="Mật khẩu">
            <Input />
          </Form.Item>
          <Form.Item name="hoTen" label="Họ tên">
            <Input />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            label="Loại người dùng"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select placeholder="Chọn loại người dùng" allowClear>
              <Option value="KhachHang">Khách hàng</Option>
              <Option value="QuanTri">Quản trị</Option>
            </Select>
          </Form.Item>
          <Form.Item name="email" label="Email">
            <Input />
          </Form.Item>
          <Form.Item name="soDT" label="Số điện thoại">
            <Input />
          </Form.Item>
          <Form.Item label="Sửa thông tin">
            <Button htmlType="submit">Xác nhận</Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
