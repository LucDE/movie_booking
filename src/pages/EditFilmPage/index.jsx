import React, { useState, useEffect } from "react";
import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Switch,
} from "antd";
import { useNavigate, useParams } from "react-router-dom";
import { moviesServ } from "services/moviesService";
import moment from "moment";
import TextArea from "antd/lib/input/TextArea";

export default function EditFilmPage() {
  const [movie, setMovie] = useState(null);
  let [image, setImage] = useState(null);
  let navigate = useNavigate();
  let params = useParams();
  let { maPhim } = params;

  let fetchMovieData = () => {
    moviesServ
      .getDetailMovie(maPhim)
      .then((res) => {
        let detailMovie = res.data.content;
        setMovie(detailMovie);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  useEffect(() => {
    fetchMovieData();
  }, []);

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    let typeOfFile = file.type;
    if (
      (typeOfFile === "image/png" || typeOfFile === "image/jpeg",
      (typeOfFile = "image/gif")) ||
      typeOfFile === "image/jpg"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.addEventListener("load", (event) => {
        const img = event.target.result;
        setImage(img);
      });
    }
  };

  const onFinish = (values) => {
    let {
      tenPhim,
      trailer,
      moTa,
      ngayKhoiChieu,
      sapChieu,
      dangChieu,
      hot,
      danhGia,
    } = values;

    let formData = new FormData();
    formData.append("maPhim", maPhim);
    formData.append("tenPhim", tenPhim);
    formData.append("trailer", trailer);
    formData.append("moTa", moTa);
    formData.append(
      "ngayKhoiChieu",
      moment(ngayKhoiChieu).format("DD/MM/YYYY")
    );
    formData.append("sapChieu", sapChieu);
    formData.append("dangChieu", dangChieu);
    formData.append("hot", hot);
    formData.append("danhGia", danhGia);

    let imageFile = document.getElementById("file").files[0];
    if (imageFile !== undefined) {
      formData.append("File", imageFile, imageFile.name);
    }

    moviesServ
      .updateMovie(formData)
      .then((res) => {
        message.success("Cập nhật phim thành công");
        setTimeout(() => {
          navigate("/user-management");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  let renderForm = () => {
    if (movie !== null) {
      return (
        <Form
          initialValues={{
            tenPhim: movie?.tenPhim,
            trailer: movie?.trailer,
            moTa: movie?.moTa,
            ngayKhoiChieu: moment(movie?.ngayKhoiChieu),
            dangChieu: movie?.dangChieu,
            sapChieu: movie?.sapChieu,
            hot: movie?.hot,
            danhGia: movie?.danhGia,
          }}
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item name="tenPhim" label="Tên phim">
            <Input />
          </Form.Item>
          <Form.Item name="trailer" label="Trailer">
            <Input />
          </Form.Item>
          <Form.Item name="moTa" label="Mô tả">
            <TextArea />
          </Form.Item>
          <Form.Item name="ngayKhoiChieu" label="Ngày khởi chiếu">
            <DatePicker />
          </Form.Item>
          <Form.Item
            name="dangChieu"
            label="Đang chiếu"
            valuePropName="checked"
          >
            <Switch style={{ border: "2px solid red" }} />
          </Form.Item>
          <Form.Item name="sapChieu" label="Sắp chiếu" valuePropName="checked">
            <Switch style={{ border: "2px solid red" }} />
          </Form.Item>
          <Form.Item name="hot" label="HOT" valuePropName="checked">
            <Switch style={{ border: "2px solid red" }} />
          </Form.Item>
          <Form.Item name="danhGia" label="Số sao">
            <InputNumber />
          </Form.Item>
          <Form.Item label="Hình Ảnh">
            <Input
              style={{ background: "transparent", border: 0 }}
              accept="image/png, image/jqg, image/gif, image/jpeg"
              type="file"
              id="file"
              onChange={(e) => {
                handleFileChange(e);
              }}
            />
          </Form.Item>
          <div className="w-2/3 flex justify-center mx-auto">
            {image === null ? (
              <img
                src={movie?.hinhAnh}
                alt="movie image"
                id="default_image"
              ></img>
            ) : (
              <img src={image} alt="upload image" />
            )}
          </div>
          <Form.Item label="Sửa phim">
            <Button htmlType="submit">Xác nhận</Button>
          </Form.Item>
        </Form>
      );
    }
  };

  return (
    <div className="container  px-10 py-32">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl text-center font-bold mb-5">EDIT FILM</h1>
        {renderForm()}
      </div>
    </div>
  );
}
