import { message, Table } from "antd";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import { userServ } from "services/userService";
import { createHeaderUsersTable } from "./userUltis";

export default function UserManagementPage() {
  const [userList, setUserList] = useState([]);

  //   console.table(userList);

  let fetchUserList = () => {
    userServ
      .getUserList()
      .then((res) => {
        let dataRaw = res.data.content;
        dataRaw.map((item) => {
          item.key = nanoid();
        });
        setUserList(dataRaw);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  };

  let handleDeleteUser = (taiKhoan) => {
    userServ
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xóa thành công");
        fetchUserList();
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log("err: ", err);
      });
  };

  useEffect(() => {
    fetchUserList();
  }, []);

  return (
    <div className="container mx-auto">
      <Table
        dataSource={userList}
        columns={createHeaderUsersTable(handleDeleteUser)}
        rowKey={(record) => record.key}
      />
    </div>
  );
}
