import { Tag, Button } from "antd";

export const createHeaderUsersTable = (onDelete) => {
  return [
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Họ tên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Số điện thoại",
      dataIndex: "soDT",
      key: "soDT",
    },
    {
      title: "Mật khẩu",
      dataIndex: "matKhau",
      key: "matKhau",
    },
    {
      title: "Loại người dùng",
      dataIndex: "maLoaiNguoiDung",
      key: "maLoaiNguoiDung",
      render: (text, user) => {
        let color = text === "QuanTri" ? "blue" : "red";
        let newName = text === "QuanTri" ? "Quản trị" : "Khách hàng";
        return <Tag color={color}>{newName}</Tag>;
      },
    },
    {
      title: "Thao tác",
      key: "action",
      render: (_, user) => {
        return (
          <div>
            <Button className="bg-blue-500 text-white">Sửa</Button>
            <Button
              className="bg-red-500 text-white"
              onClick={() => {
                onDelete(user.taiKhoan);
              }}
            >
              Xóa
            </Button>
          </div>
        );
      },
    },
  ];
};
