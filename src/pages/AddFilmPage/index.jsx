import React, { useState } from "react";
import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  message,
  Switch,
} from "antd";
import { moviesServ } from "services/moviesService";
import moment from "moment";
import { useNavigate } from "react-router-dom";

export default function AddFilmPage() {
  let [image, setImage] = useState(null);
  let navigate = useNavigate();
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    let typeOfFile = file.type;
    if (
      (typeOfFile === "image/png" || typeOfFile === "image/jpeg",
      typeOfFile === "image/gif") ||
      typeOfFile === "image/jpg"
    ) {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.addEventListener("load", (event) => {
        const img = event.target.result;
        setImage(img);
      });
    }
  };
  const onFinish = (values) => {
    let {
      tenPhim,
      trailer,
      moTa,
      ngayKhoiChieu,
      sapChieu,
      dangChieu,
      hot,
      danhGia,
    } = values;

    let formData = new FormData();
    formData.append("maPhim", Math.floor(Math.random() * 99999));
    formData.append("tenPhim", tenPhim);
    formData.append("trailer", trailer);
    formData.append("moTa", moTa);
    formData.append(
      "ngayKhoiChieu",
      moment(ngayKhoiChieu).format("DD/MM/YYYY")
    );
    formData.append("sapChieu", sapChieu);
    formData.append("dangChieu", dangChieu);
    formData.append("hot", hot);
    formData.append("danhGia", danhGia);

    let imageFile = document.getElementById("file").files[0];
    formData.append("File", imageFile, imageFile.name);

    moviesServ
      .addMovie(formData)
      .then((res) => {
        message.success("Thêm phim thành công");
        setTimeout(() => {
          navigate("/user-management");
        }, 1000);
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  return (
    <div className="container  px-10 py-32">
      <div className="border-4 border-red-500 py-5 bg-slate-500">
        <h1 className="text-3xl font-bold text-center mb-5">ADD FILM</h1>

        <Form
          id="form"
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 14,
          }}
          layout="horizontal"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item name="tenPhim" label="Tên phim">
            <Input />
          </Form.Item>
          <Form.Item name="trailer" label="Trailer">
            <Input />
          </Form.Item>
          <Form.Item name="moTa" label="Mô tả">
            <Input />
          </Form.Item>
          <Form.Item name="ngayKhoiChieu" label="Ngày khởi chiếu">
            <DatePicker />
          </Form.Item>
          <Form.Item
            name="dangChieu"
            label="Đang chiếu"
            valuePropName="checked"
          >
            <Switch style={{ border: "2px solid red" }} />
          </Form.Item>
          <Form.Item name="sapChieu" label="Sắp chiếu" valuePropName="checked">
            <Switch style={{ border: "2px solid red" }} />
          </Form.Item>
          <Form.Item name="hot" label="HOT" valuePropName="checked">
            <Switch style={{ border: "2px solid red" }} />
          </Form.Item>
          <Form.Item name="danhGia" label="Số sao">
            <InputNumber />
          </Form.Item>
          <Form.Item label="Hình ảnh">
            <Input
              style={{ background: "transparent", border: 0 }}
              accept="image/png, image/jqg, image/gif, image/jpeg"
              type="file"
              id="file"
              onChange={(e) => {
                handleFileChange(e);
              }}
            />
          </Form.Item>
          <div>
            <img src={image} alt="" />
          </div>
          <Form.Item label="Thêm phim">
            <Button htmlType="submit">Xác nhận</Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
