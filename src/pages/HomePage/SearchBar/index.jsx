import React, { useState, useEffect } from "react";
import { message, Select } from "antd";
import axios from "axios";
import moment from "moment";
import swal from "sweetalert";
import { moviesServ } from "services/moviesService";
import { BASE_URL, configHeader } from "services/url.config";
import { useNavigate } from "react-router-dom";

export default function SearchBar() {
  let [movieOptions, setMovieOptions] = useState([]);
  let [cinemaOptions, setCinemaOptions] = useState([]);
  let [scheduleOptions, setScheduleOptions] = useState([]);

  let [listCinema, setListCinema] = useState([]);

  let [currentSelectedMovie, setCurrentSelectedMovie] = useState("Phim");
  let [currentSelectedCinema, setCurrentSelectedCinema] = useState("Rạp");
  let [currentSelectedSchedule, setCurrentSelectedSchedule] =
    useState("Ngày giờ chiếu");

  let navigate = useNavigate();

  const handleSelectMovie = (value) => {
    fetchListCinema(value);
    setCurrentSelectedMovie(value);
    setCurrentSelectedCinema("Rạp");
  };

  const handleSelectCinema = (value) => {
    let lichChieuPhim = [];
    listCinema.heThongRapChieu.forEach((item) => {
      item.cumRapChieu.forEach((rap) => {
        if (rap.maCumRap === value) {
          lichChieuPhim = rap.lichChieuPhim;
        }
      });
    });
    if (lichChieuPhim.length !== 0) {
      let optionArray = [
        {
          value: "defaultValue",
          disabled: true,
          label: "Ngày giờ chiếu",
        },
      ];
      lichChieuPhim.forEach((lichChieu) => {
        optionArray.push({
          value: lichChieu.maLichChieu,
          label: moment(lichChieu.ngayChieuGioChieu).format(
            "DD-MM-YYYY - HH:MM"
          ),
        });
      });
      setScheduleOptions(optionArray);
    }
    setCurrentSelectedCinema(value);
    setCurrentSelectedSchedule("Ngày giờ chiếu");
  };

  const handleSelectSchedule = (value) => {
    setCurrentSelectedSchedule(value);
  };

  const fetchListMovie = async () => {
    try {
      let res = await moviesServ.getMovieList();
      let result = res.data.content;
      if (result.length !== 0) {
        let optionArray = [];
        result.forEach((item) => {
          optionArray.push({
            value: item.maPhim,
            label: item.tenPhim,
          });
        });

        setMovieOptions(optionArray);
      }
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  const fetchListCinema = async (value) => {
    try {
      let res = await axios({
        url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${value}`,
        method: "GET",
        headers: configHeader(),
      });
      let result = res.data.content;
      setListCinema(result);
      if (result.length !== 0) {
        let optionArray = [
          {
            value: "defaultValue",
            disabled: true,
            label: "Rạp",
          },
        ];

        result.heThongRapChieu.forEach((item) => {
          item.cumRapChieu.forEach((rap) => {
            optionArray.push({
              value: rap.maCumRap,
              label: rap.tenCumRap,
            });
          });
        });
        setCinemaOptions(optionArray);
      }
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  const handlePurchase = () => {
    if (currentSelectedMovie === "Phim") {
      swal({
        icon: "info",
        title: "Bạn chưa chọn phim",
        text: "Vui lòng chọn phim",
      });
      return;
    }
    if (currentSelectedCinema === "Rạp") {
      swal({
        icon: "info",
        title: "Bạn chưa chọn rạp",
        text: "Vui lòng chọn rạp",
      });
      return;
    }
    if (currentSelectedSchedule === "Ngày giờ chiếu") {
      swal({
        icon: "info",
        title: "Bạn chưa chọn ngày giờ chiếu",
        text: "Vui lòng chọn ngày giờ chiếu",
      });
      return;
    }
    if (
      currentSelectedMovie !== "Phim" &&
      currentSelectedCinema !== "Rạp" &&
      currentSelectedSchedule !== "Ngày giờ chiếu"
    ) {
      navigate(`/purchase/${currentSelectedSchedule}`);
    }
  };

  useEffect(() => {
    fetchListMovie();
  }, []);

  return (
    <section
      className="search__bar df:pt-28 sm:pt-0 lg:w-10/12 lg: lg:pt-0 lg:-top-8 xl:w-2/3 mx-auto relative"
      id="search__bar"
    >
      <div className="flex justify-center df:hidden sm:hidden lg:block">
        <div className="flex relative px-8">
          <div className="flex lg:w-full bg-white drop-shadow-lg df:p-3 xl:p-5 rounded">
            <Select
              className="w-1/4 flex items-center mr-1 text-xl"
              defaultValue="Phim"
              onChange={handleSelectMovie}
              options={movieOptions}
              id="currentMovie"
            />
            <Select
              className="w-1/4 flex items-center mr-1 text-xl"
              defaultValue="Rạp"
              value={currentSelectedCinema}
              onChange={handleSelectCinema}
              options={cinemaOptions}
            />
            <Select
              className="w-1/4 flex items-center mr-1 text-xl"
              defaultValue="Ngày giờ chiếu"
              value={currentSelectedSchedule}
              onChange={handleSelectSchedule}
              options={scheduleOptions}
            />
            <div className="w-1/4 h-full flex justify-center">
              <button
                onClick={handlePurchase}
                className="bg-amber-500 p-3 rounded text-white font-bold hover:bg-red-500 "
              >
                MUA VÉ NGAY
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
