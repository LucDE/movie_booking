import React, { useState, useEffect } from "react";
import { moviesServ } from "services/moviesService";
import { Carousel, message } from "antd";
import { PrevArrow } from "components/CustomArrow/CustomPrevArrow";
import { NextArrow } from "components/CustomArrow/CustomNextArrow";
import { openModalTrailer } from "redux/slice/modalSlice";
import { useDispatch } from "react-redux";
import { btnVideoTrailer } from "assets/btnVideoTrailer";
import "./index.scss";

export default function Banners() {
  let [isShowing, setIsShowing] = useState(false);

  let [bannersList, setBannersList] = useState([]);
  const dispatch = useDispatch();

  const getBannersList = async () => {
    try {
      let res = await moviesServ.getBannerListServ();
      let bannersListServ = res.data.content;
      let trailerId = ["uqJ9u7GSaYM", "kBY2k3G6LsM", "NYH2sLid0Zc"];
      trailerId.forEach((id, index) => {
        bannersListServ[index].idVideo = id;
      });
      setBannersList(bannersListServ);
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  useEffect(() => {
    getBannersList();
  }, []);

  const showBtnTrailer = () => {
    setIsShowing(true);
  };
  const hideBtnTrailer = () => {
    setIsShowing(false);
  };

  const renderBannersList = () => {
    return bannersList.map((banner) => {
      return (
        <div key={banner.maPhim}>
          <div
            className="flex sm:h-300 md:h-400 lg:h-500 xl:h-700 justify-center items-center"
            style={{
              position: "relative",
              background: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)),url(${banner.hinhAnh})`,
              backgroundPosition: "left top",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
            }}
          >
            {isShowing ? (
              <button
                className="btn_video_trailer opacity-60 hover:opacity-100"
                onClick={() => {
                  dispatch(openModalTrailer(banner.idVideo));
                }}
              >
                <img src={btnVideoTrailer} alt="video-popup-button" />
              </button>
            ) : (
              <button
                className="btn_video_trailer opacity-60 hover:opacity-100 hidden"
                onClick={() => {
                  dispatch(openModalTrailer(banner.idVideo));
                }}
              >
                <img src={btnVideoTrailer} alt="video-popup-button" />
              </button>
            )}
          </div>
        </div>
      );
    });
  };

  return (
    <div
      className="df:hidden sm:block"
      id="banners"
      onMouseOver={showBtnTrailer}
      onMouseLeave={hideBtnTrailer}
    >
      <Carousel
        autoplay
        arrows={true}
        prevArrow={<PrevArrow />}
        nextArrow={<NextArrow />}
      >
        {renderBannersList()}
      </Carousel>
    </div>
  );
}
