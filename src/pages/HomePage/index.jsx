import React from "react";
import Banners from "./Banners";
import MoviesList from "./MoviesList";
import Footer from "./Footer";
import MovieTabs from "./MoviesTab";
import SearchBar from "./SearchBar";

export default function Homepage() {
  return (
    <div>
      <Banners />
      <SearchBar />
      <MoviesList />
      <MovieTabs />
      <Footer />
    </div>
  );
}
