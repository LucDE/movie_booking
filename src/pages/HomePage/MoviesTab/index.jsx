import { message, Tabs } from "antd";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import MovieTabItems from "./MovieTabItems";
import { moviesServ } from "services/moviesService";
import "./index.scss";

export default function MovieTabs() {
  let [dataRaw, setDataRaw] = useState([]);

  const fetchDataMovies = async () => {
    try {
      let res = await moviesServ.getMoviesByTheater();
      setDataRaw(res.data.content);
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  useEffect(() => {
    fetchDataMovies();
  }, []);

  let renderHeThongRap = () => {
    return dataRaw.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={
            <div className="mx-2 border-b-2 p-2 ">
              <img
                style={{ height: 80, width: 80 }}
                src={heThongRap.logo}
                alt=""
              />
            </div>
          }
          key={index}
        >
          <Tabs style={{ height: 720 }} tabPosition="left" defaultActiveKey="1">
            {heThongRap.lstCumRap.map((cumRap) => {
              return (
                <Tabs.TabPane
                  style={{ height: 720 }}
                  className="text-left overflow-y-scroll"
                  key={cumRap.maCumRap}
                  tab={
                    <div
                      style={{ height: 85 }}
                      className="text-left border-b-2 p-2 mx-3"
                    >
                      <div>
                        <p className="text-amber-500 font-bold uppercase ">
                          {cumRap.tenCumRap.length > 22
                            ? cumRap.tenCumRap.substring(0, 22) + "..."
                            : cumRap.tenCumRap}
                        </p>
                        <p>
                          {cumRap.diaChi.length > 30
                            ? cumRap.diaChi.substring(0, 30) + "..."
                            : cumRap.diaChi}
                        </p>
                        <p className="text-red-500">Xem chi tiết</p>
                      </div>
                    </div>
                  }
                >
                  {cumRap.danhSachPhim.map((item) => {
                    return <MovieTabItems key={nanoid()} movie={item} />;
                  })}
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <section className="movietabs df:hidden lg:block space-y-8 py-10">
      <div>
        <p className="text-center t text-3xl font-bold" id="comming__movies">
          Lịch chiếu phim
        </p>
      </div>
      <div className="container flex justify-center lg:px-10 mx-auto">
        <Tabs
          className="lg:w-full xl:w-4/6 border-4 rounded-lg"
          tabPosition="left"
          defaultActiveKey="1"
        >
          {renderHeThongRap()}
        </Tabs>
      </div>
    </section>
  );
}
