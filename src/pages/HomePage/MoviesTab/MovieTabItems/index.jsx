import React from "react";
import { NavLink } from "react-router-dom";

export default function MovieTabItems({ movie }) {
  return (
    <div key={movie.maPhim} className=" movie__container flex border-b-2">
      <div className="w-1/6 movie__pic p-3">
        <img
          className="object-cover rounded-lg"
          src={movie.hinhAnh}
          alt="vip_image"
        />
      </div>
      <div className="w-5/6 movie__detail p-3 space-y-4">
        <div className="space-x-2">
          <span className="text-2xl font-bold">
            {movie.tenPhim}
            {movie.hot === true ? <> 🔥</> : ""}
          </span>
        </div>
        <div className="grid grid-cols-2 gap-4">
          {movie.lstLichChieuTheoPhim.map((lichChieu) => {
            return (
              <div key={lichChieu.maLichChieu}>
                <NavLink
                  to={`/purchase/${lichChieu.maLichChieu}`}
                  className="border-solid border  space-x-2 border-gray-500 rounded p-1"
                >
                  <span className="text-red-500 font-semibold">
                    {lichChieu.ngayChieuGioChieu.substring(0, 10)}
                  </span>
                  <span>~</span>
                  <span className="text-amber-500 font-semibold">
                    {lichChieu.ngayChieuGioChieu.substring(12, 19)}
                  </span>
                </NavLink>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
