import React, { useState, useEffect } from "react";
import { Carousel, message } from "antd";
import ItemMovie from "./ItemMovie";
import { nanoid } from "nanoid";
import { moviesServ } from "services/moviesService";
import phanTrang from "services/panigatePages";
import SearchFilm from "./SearchFilm";
import "./index.scss";
import _ from "lodash";

export default function MoviesList() {
  let [dataRaw, setDataRaw] = useState([]);

  let [memoryDataRaw, setMemoryDataRaw] = useState([]);

  let [noData, setNoData] = useState(false);

  const fetchDataMovies = async () => {
    try {
      let res = await moviesServ.getMovieList();
      setDataRaw(res.data.content);
      setMemoryDataRaw(res.data.content);
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  const handleSearch = (value) => {
    let specialCharacter =
      /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/;
    if (value.trim() === "") {
      setDataRaw(memoryDataRaw);
      setNoData(false);
      return;
    }
    if (specialCharacter.test(value.trim())) {
      setDataRaw([]);
      setNoData(true);
      return;
    }
    let cloneDataRaw = _.cloneDeep(memoryDataRaw);
    let newData = [...cloneDataRaw].filter((item) => {
      return (
        item.tenPhim.toUpperCase().search(value.trim().toUpperCase()) !== -1
      );
    });
    if (newData.length !== 0) {
      setNoData(false);
      setDataRaw(newData);
    } else {
      setDataRaw([]);
      setNoData(true);
    }
  };

  useEffect(() => {
    fetchDataMovies();
  }, []);

  let renderMovieList = () => {
    let cloneDataRaw = [...dataRaw];
    let listMovieWithPages = phanTrang(cloneDataRaw, 8);
    return listMovieWithPages.map((arrayMovie) => {
      return (
        <div key={nanoid()}>
          <div className="container flex justify-center df:p-10 sm:py-0 mx-auto">
            <div className="grid df:w-full df:grid-cols-1 sm:w-full sm:grid-cols-3 lg:w-full lg:grid-cols-4 xl:w-2/3 gap-8">
              {arrayMovie.map((item) => {
                return <ItemMovie dataMovie={item} key={nanoid()} />;
              })}
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <div className="df:pb-5 sm:pb-0" id="movies__list">
      <div className="df:block lg:hidden sm:mb-10">
        <SearchFilm handleSearch={handleSearch} />
      </div>
      <Carousel>{renderMovieList()}</Carousel>
      <div className="flex justify-center py-5">
        {noData && <span>Không tìm thấy phim có từ khóa này</span>}
      </div>
    </div>
  );
}
