import React from "react";
import { Input } from "antd";
const { Search } = Input;
export default function SearchFilm({ handleSearch }) {
  return (
    <div className="px-10 sm:pt-10">
      <div className="shadow-md">
        <Search
          onSearch={handleSearch}
          size="large"
          placeholder="Tìm kiếm phim..."
          enterButton={
            <div className="border-0">
              <span className="text-black">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width={24}
                  height={24}
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="#000000"
                  strokeWidth={2}
                  strokeLinecap="round"
                  strokeLinejoin="round"
                >
                  <circle cx={11} cy={11} r={8} />
                  <line x1={21} y1={21} x2="16.65" y2="16.65" />
                </svg>
              </span>
            </div>
          }
        />
      </div>
    </div>
  );
}
