import React from "react";
import "./index.scss";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function CardMobile({ dataMovie }) {
  let customStyle = {
    background: `url(${dataMovie.hinhAnh}) left top / cover no-repeat`,
    borderRadius: 4,
  };
  return (
    <Card
      style={{ cursor: "pointer" }}
      className="w-full flex rounded"
      cover={
        <div
          className="h-48 mr-5 sm:mr-0 flex justify-center items-center"
          style={customStyle}
        ></div>
      }
    >
      <Meta
        title={
          <p className="ml-3 text-blue-800 uppercase font-bold">
            {dataMovie.tenPhim.length < 20
              ? dataMovie.tenPhim
              : dataMovie.tenPhim.substring(0, 20) + "..."}
          </p>
        }
        description={
          <div className="ml-3">
            <div className="h-24">
              <span className="text-gray-500 ">
                {dataMovie.moTa.substring(0, 50) + "..."}
              </span>
            </div>
            <div className="my-2 block">
              <NavLink
                className="block w-full p-4 bg-amber-500 rounded text-center text-white font-bold uppercase hover:text-white hover:bg-amber-600"
                to={`detail/${dataMovie.maPhim}`}
              >
                Đặt vé
              </NavLink>
            </div>
          </div>
        }
      />
    </Card>
  );
}
