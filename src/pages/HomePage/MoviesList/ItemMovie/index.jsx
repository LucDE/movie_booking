import React from "react";
import CardMobile from "./CardMobile";
import CardDesktop from "./CardDesktop";
import { DesktopReponsive, MobileReponsive } from "HOC/Reponsive";

export default function ItemMovie({ dataMovie }) {
  return (
    <>
      <MobileReponsive>
        <CardMobile dataMovie={dataMovie} />
      </MobileReponsive>
      <DesktopReponsive>
        <CardDesktop dataMovie={dataMovie} />
      </DesktopReponsive>
    </>
  );
}
