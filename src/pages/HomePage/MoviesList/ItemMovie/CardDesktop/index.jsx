import React, { useState } from "react";
import "./index.scss";
import { useDispatch } from "react-redux";
import { openModalTrailer } from "redux/slice/modalSlice";
import { btnVideoTrailer } from "assets/btnVideoTrailer";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function CardDesktop({ dataMovie }) {
  let [isShowing, setIsShowing] = useState(false);
  let [customStyle, setCustomStyle] = useState({
    background: `url(${dataMovie.hinhAnh}) top / cover no-repeat`,
    borderRadius: 4,
  });
  let dispatch = useDispatch();
  const showBtnTrailer = () => {
    if (isShowing === false) {
      setIsShowing(true);
    } else {
      return;
    }
    setCustomStyle({
      background: `linear-gradient(to top,#000,transparent 100%),url(${dataMovie.hinhAnh}) top / cover no-repeat`,
      borderRadius: 4,
    });
  };
  const hideBtnTrailer = () => {
    setIsShowing(false);
    setCustomStyle({
      background: `url(${dataMovie.hinhAnh}) top / cover no-repeat`,
      borderRadius: 4,
    });
  };
  return (
    <Card
      onMouseOver={showBtnTrailer}
      onMouseLeave={hideBtnTrailer}
      style={{ cursor: "pointer" }}
      className="block rounded-lg"
      cover={
        <div
          className="mr-0 h-72 flex justify-center items-center"
          style={customStyle}
        >
          {isShowing && (
            <button
              className="btn_video_trailer"
              onClick={() => {
                dispatch(openModalTrailer(dataMovie.trailer.substr(-11)));
              }}
            >
              <img width={60} src={btnVideoTrailer} alt="video-popup-button" />
            </button>
          )}
        </div>
      }
    >
      {isShowing ? (
        <NavLink
          className="block w-full bg-amber-500 p-3 rounded text-center text-white font-bold uppercase hover:text-white hover:bg-amber-600"
          to={`detail/${dataMovie.maPhim}`}
        >
          Đặt vé
        </NavLink>
      ) : (
        <Meta
          className="h-20"
          title={
            <p className=" text-blue-800 uppercase font-bold">
              {dataMovie.tenPhim.length < 20
                ? dataMovie.tenPhim
                : dataMovie.tenPhim.substring(0, 20) + "..."}
            </p>
          }
          description={
            <div>
              <div>
                <span className="text-gray-500 ">
                  {dataMovie.moTa.substring(0, 50) + "..."}
                </span>
              </div>
            </div>
          }
        />
      )}
    </Card>
  );
}
