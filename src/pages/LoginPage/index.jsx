import React, { useEffect } from "react";
import { Button, Form, Input, message } from "antd";
import { LockOutlined, UserOutlined, LoginOutlined } from "@ant-design/icons";
import { NavLink, useNavigate } from "react-router-dom";
import { userInforLocal } from "../../services/local.service";
import { useDispatch } from "react-redux";
import { setUserInfor } from "../../redux/slice/userSlice";
import { userServ } from "../../services/userService";
import "./index.scss";

const styleLoginPage = {
  backgroundImage:
    "linear-gradient(to top,#000,transparent 100%),url(https://images.hdqwalls.com/wallpapers/minions-the-rise-of-gru-movie-kf.jpg)",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
};

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  useEffect(() => {
    let userInfor = userInforLocal.get();
    if (userInfor) {
      navigate("/");
    }
  }, []);

  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        userInforLocal.set(res.data.content);
        dispatch(setUserInfor(res.data.content));
        message.success("Đăng nhập thành công");
        let loaiNguoiDung = res.data.content.maLoaiNguoiDung;
        if (loaiNguoiDung === "QuanTri") {
          setTimeout(() => {
            navigate("/user-management");
          }, 1000);
        } else {
          setTimeout(() => {
            navigate("/");
          }, 1000);
        }
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <div
      style={styleLoginPage}
      className="h-screen w-screen flex justify-center items-center p-5"
      id="login__page"
    >
      <div className="container mx-auto p-5 rounded-lg flex justify-center">
        <div className="df:w-full lg:w-2/5 bg-slate-200 p-5 rounded-xl ">
          <div className="flex flex-col items-center mb-5">
            <LoginOutlined className="text-4xl text-red-500" />
            <h4 className="text-xl font-bold">Đăng nhập</h4>
          </div>
          <Form
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Trường này yêu cầu nhập!",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Tài khoản"
              />
            </Form.Item>

            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Trường này yêu cầu nhập!",
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="Mật khẩu"
              />
            </Form.Item>
            <div className="flex justify-center">
              <Form.Item>
                <Button
                  className="bg-red-500  text-white rounded"
                  htmlType="submit"
                >
                  Đăng nhập
                </Button>
              </Form.Item>
            </div>
            <div className="flex justify-end">
              <NavLink
                className="text-red-500 text-base font-bold underline"
                to="/sign-up"
              >
                Bạn chưa có tài khoản? Đăng ký
              </NavLink>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
