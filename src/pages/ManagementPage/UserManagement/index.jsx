import React, { useEffect, useState } from "react";
import { message, Table } from "antd";
import { createHeaderUsersTable } from "./userUltis";
import { userServ } from "services/userService";
import { Input } from "antd";
import { NavLink } from "react-router-dom";
import _ from "lodash";
const { Search } = Input;

export default function UserManagement() {
  const [dataRaw, setDataRaw] = useState([]);
  const [memoryDataRaw, setMemoryDataRaw] = useState([]);

  let handleDeleteUser = (taiKhoan) => {
    userServ
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xóa user thành công");
        fetchUserList();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  let handleSearchUser = (value) => {
    let specialCharacter =
      /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/;
    if (value.trim() === "") {
      setDataRaw(memoryDataRaw);
      return;
    }
    if (specialCharacter.test(value.trim())) {
      setDataRaw([]);
      return;
    }
    let cloneDataRaw = _.cloneDeep(memoryDataRaw);
    let newData = [...cloneDataRaw].filter((item) => {
      return item.hoTen.toUpperCase().search(value.trim().toUpperCase()) !== -1;
    });
    if (newData.length !== 0) {
      setDataRaw(newData);
    } else {
      setDataRaw([]);
    }
  };

  let fetchUserList = () => {
    userServ
      .getUserList()
      .then((res) => {
        let newData = res.data.content;
        newData.forEach((item, index) => {
          item.key = index + 1;
        });
        setDataRaw(newData);
        setMemoryDataRaw(newData);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    fetchUserList();
  }, []);

  return (
    <>
      <div className="flex justify-center">
        <NavLink className=" bg-amber-500 py-2 px-20 rounded" to="/add-user">
          Thêm người dùng
        </NavLink>
      </div>
      <div className="px-10 py-10 ">
        <div className="shadow-md">
          <Search
            onSearch={handleSearchUser}
            size="large"
            placeholder="Tìm kiếm người dùng..."
            enterButton={
              <div className="border-0">
                <span className="text-black">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#000000"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <circle cx={11} cy={11} r={8} />
                    <line x1={21} y1={21} x2="16.65" y2="16.65" />
                  </svg>
                </span>
              </div>
            }
          />
        </div>
      </div>
      <Table
        dataSource={dataRaw}
        columns={createHeaderUsersTable(handleDeleteUser)}
        rowKey={(record) => record.key}
      />
    </>
  );
}
