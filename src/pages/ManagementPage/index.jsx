import { Tabs } from "antd";
import React from "react";
import FilmManagement from "./FilmManagement";
import UserManagement from "./UserManagement";

export default function ManagementPage() {
  //   console.table(userList);
  return (
    <div className="container  px-10 py-32">
      <div className="border-2 border-black p-3">
        <Tabs tabPosition="top">
          <Tabs.TabPane
            tab={<p className="font-bold">Quản lý người dùng</p>}
            key="item-tab-1"
          >
            <UserManagement />
          </Tabs.TabPane>
          <Tabs.TabPane
            tab={<p className="font-bold">Quản lý phim</p>}
            key="item-tab-2"
          >
            <FilmManagement />
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  );
}

//tách components film table & user table
