import React, { useEffect, useState } from "react";
import { message, Table } from "antd";
import _ from "lodash";
import { createHeaderFilmsTable } from "./filmUltis";
import { moviesServ } from "services/moviesService";
import { Input } from "antd";
import { NavLink } from "react-router-dom";
const { Search } = Input;

export default function FilmManagement() {
  const [dataRaw, setDataRaw] = useState([]);
  const [memoryDataRaw, setMemoryDataRaw] = useState([]);

  let handleDeleteFilm = (maPhim) => {
    moviesServ
      .deleteMovie(maPhim)
      .then((res) => {
        message.success("Xóa film thành công");
        fetchMoviesList();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  let handleSearchFilm = (value) => {
    let specialCharacter =
      /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/;
    if (value.trim() === "") {
      setDataRaw(memoryDataRaw);
      return;
    }
    if (specialCharacter.test(value.trim())) {
      setDataRaw([]);
      return;
    }
    let cloneDataRaw = _.cloneDeep(memoryDataRaw);
    let newData = [...cloneDataRaw].filter((item) => {
      return (
        item.tenPhim.toUpperCase().search(value.trim().toUpperCase()) !== -1
      );
    });
    if (newData.length !== 0) {
      setDataRaw(newData);
    } else {
      setDataRaw([]);
    }
  };

  let fetchMoviesList = () => {
    moviesServ
      .getMovieList()
      .then((res) => {
        let dataRaw = res.data.content;
        dataRaw.forEach((item, index) => {
          item.key = index + 1;
        });
        setDataRaw(dataRaw);
        setMemoryDataRaw(dataRaw);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    fetchMoviesList();
  }, []);

  return (
    <>
      <div className="flex justify-center">
        <NavLink className=" bg-amber-500 py-2 px-20 rounded" to="/add-film">
          Thêm phim
        </NavLink>
      </div>
      <div className="px-10 py-10 ">
        <div className="shadow-md">
          <Search
            onSearch={handleSearchFilm}
            size="large"
            placeholder="Tìm kiếm phim..."
            enterButton={
              <div className="border-0">
                <span className="text-black">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#000000"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <circle cx={11} cy={11} r={8} />
                    <line x1={21} y1={21} x2="16.65" y2="16.65" />
                  </svg>
                </span>
              </div>
            }
          />
        </div>
      </div>
      <Table
        dataSource={dataRaw}
        columns={createHeaderFilmsTable(handleDeleteFilm)}
        rowKey={(record) => record.key}
      />
    </>
  );
}
