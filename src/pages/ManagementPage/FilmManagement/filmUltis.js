import { Button } from "antd";
import { NavLink } from "react-router-dom";

export const createHeaderFilmsTable = (onDelete) => {
  return [
    {
      title: "STT",
      dataIndex: "key",
      key: "stt",
    },
    {
      title: "Mã phim",
      dataIndex: "maPhim",
      key: "maPhim",
    },
    {
      title: "Hình ảnh",
      key: "hinhAnh",
      render: ({ hinhAnh }) => {
        return <img width="100" alt={hinhAnh} src={hinhAnh}></img>;
      },
    },
    {
      title: "Tên phim",
      dataIndex: "tenPhim",
      key: "tenPhim",
    },
    {
      title: "Mô tả",
      key: "moTa",
      render: ({ moTa }) => {
        return <span>{moTa.substring(0, 50) + "..."}</span>;
      },
    },
    {
      title: "Thao tác",
      key: "action",
      render: ({ maPhim }) => {
        return (
          <div className="space-x-4">
            <Button className="bg-blue-500 text-white rounded">
              <NavLink to={`/edit-film/${maPhim}`}>Sửa</NavLink>
            </Button>
            <Button
              className="bg-red-500 text-white  rounded"
              onClick={() => {
                onDelete(maPhim);
              }}
            >
              Xóa
            </Button>
          </div>
        );
      },
    },
  ];
};
