import React, { useEffect, useState } from "react";
import "./index.scss";
import { NavLink, useParams } from "react-router-dom";
import { moviesServ } from "services/moviesService";
import { message, Tabs } from "antd";
import { nanoid } from "nanoid";

export default function MovieSchedules() {
  let [dataRaw, setDataRaw] = useState([]);

  let param = useParams();
  let { id } = param;

  let fetchData = async () => {
    try {
      let res = await moviesServ.getMovieScheduleServ(id);
      setDataRaw(res.data.content);
    } catch (err) {
      message.error(err.response.data.content);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  let renderHeThongRap = () => {
    if (dataRaw.length !== 0) {
      if (dataRaw.heThongRapChieu.length !== 0) {
        return dataRaw.heThongRapChieu.map((heThongRap) => {
          return (
            <Tabs.TabPane
              tab={
                <div key={nanoid()} className="p-2 border-b-2">
                  <img
                    className="df:w-10 df:h-10 sm:w-20 sm:h-20"
                    src={heThongRap.logo}
                    alt={heThongRap.logo}
                  />
                </div>
              }
              key={nanoid()}
            >
              {heThongRap.cumRapChieu.map((movie) => {
                return (
                  <div
                    key={nanoid()}
                    className=" movie__container flex border-b-2"
                  >
                    <div className="movie__detail p-3 space-y-4">
                      <div className="space-x-2">
                        <span className="df:text-xs sm:text-base md:text-xl lg:text-2xl font-bold">
                          {movie.tenCumRap}
                        </span>
                      </div>
                      <div className="grid df:grid-cols-1 df:gap-4 sm:grid-cols-2 md:grid-cols-2 md:gap-2 lg:grid-cols-3 lg:gap-2">
                        {movie.lichChieuPhim.map((lichChieu) => {
                          return (
                            <div key={nanoid()}>
                              <NavLink
                                to={`/purchase/${lichChieu.maLichChieu}`}
                                className="border-solid border  space-x-2 border-gray-500 rounded p-1"
                              >
                                <span className="df:text-xs sm:text-base text-red-500 font-semibold">
                                  {lichChieu.ngayChieuGioChieu.substring(0, 10)}
                                </span>
                                <span>~</span>
                                <span className="df:text-xs sm:text-base text-amber-500 font-semibold">
                                  {lichChieu.ngayChieuGioChieu.substring(
                                    12,
                                    19
                                  )}
                                </span>
                              </NavLink>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                );
              })}
            </Tabs.TabPane>
          );
        });
      } else {
        return (
          <Tabs.TabPane
            tab={
              <div className="p-2 border-b-2">
                <span>Thông tin</span>
              </div>
            }
          >
            <div className="flex justify-center items-center py-2 ">
              Hiện tại phim chưa có xuất chiếu
            </div>
          </Tabs.TabPane>
        );
      }
    }
  };
  return (
    <section className="movietabs space-y-5 df:py-0 sm:py-10">
      <div className="df:hidden sm:hidden lg:block">
        <p className="text-center text-amber-500 text-3xl font-bold">
          Lịch chiếu phim
        </p>
      </div>
      <div className="container flex justify-center p-5 mx-auto">
        <Tabs
          className="df:w-full md:w-5/6 lg:w-5/6 xl:w-4/6 border-4 rounded-lg"
          tabPosition="left"
          defaultActiveKey="1"
        >
          {renderHeThongRap()}
        </Tabs>
      </div>
    </section>
  );
}
