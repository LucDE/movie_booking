import React, { useEffect, useState } from "react";
import { Progress } from "antd";
import { useParams } from "react-router-dom";
import { moviesServ } from "services/moviesService";
import MovieSchedules from "./MovieSchedules";
import moment from "moment";
import "./index.scss";

export default function DetailPage() {
  const [movie, setMovie] = useState({});

  let param = useParams();
  let { id } = param;

  let fetchMovieData = () => {
    moviesServ
      .getDetailMovie(id)
      .then((res) => {
        let detailMovie = res.data.content;
        setMovie(detailMovie);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    fetchMovieData();
  }, []);

  const renderStar = (number) => {
    return (
      <span className="text-yellow-300 space-x-1">
        {renderIcon(Math.floor(number / 2))}
      </span>
    );
  };
  const renderIcon = (number) => {
    let arrayRender = [];
    for (let i = 0; i < number; i++) {
      arrayRender.push("");
    }
    return arrayRender.map((item, index) => {
      return <i key={index} className="fa fa-star"></i>;
    });
  };
  return (
    <div>
      <div className="df:hidden md:block">
        <div
          style={{
            backgroundImage: `linear-gradient(to right,rgba(0,0,0,1)150px,rgba(0,0,0,.6)100%),url(${movie.hinhAnh})`,
            backgroundPosition: "center",
            backgroundSize: "contain",
            backgroundRepeat: "repeat-x",
          }}
          className="detail__page flex justify-center py-36"
        >
          <div className=" md:w-5/6 xl:w-4/6 flex md:space-x-4 justify-around items-start">
            <div className="md:w-2/6 lg:w-3/12 h-full">
              <img
                className="rounded-lg w-full h-full"
                src={movie.hinhAnh}
                alt=""
              />
            </div>
            <div className="md:w-3/6 md:space-y-8 lg:w-6/12 lg:space-y-4 ">
              <h1 className="md:text-xl lg:text-3xl text-white">
                {movie.tenPhim}
              </h1>
              <div className="md:hidden lg:block">
                <h2 className="text-2xl text-white">Nội dung</h2>
                <h3 className=" text-gray-400"> {movie.moTa}</h3>
              </div>
              <div>
                <span className="lg:hidden md:text-base md:text-white ">
                  Ngày khởi chiếu:
                  {" " + moment(movie.ngayChieuGioChieu).format("DD-MM-YYYY")}
                </span>
              </div>
              <div>
                <span className="lg:hidden md:text-base md:text-white ">
                  Giờ chiếu:
                  {" " + moment(movie.ngayChieuGioChieu).format("HH:MM")}
                </span>
              </div>
              <div>
                <a
                  href="#movieSchedule"
                  className="block md:w-full lg:w-1/3 text-center uppercase font-bold bg-red-500 hover:bg-orange-500 text-white px-5 py-2 rounded-lg"
                >
                  Đặt vé
                </a>
              </div>
            </div>
            <div className="md:w-1/6 lg:w-3/12 flex flex-col items-center space-y-2">
              <Progress
                type="circle"
                strokeColor="red"
                format={(number) => {
                  return (
                    <span className="text-yellow-300">
                      {number / 10 + " điểm"}
                    </span>
                  );
                }}
                percent={movie.danhGia * 10}
              />
              <div>{renderStar(movie.danhGia)}</div>
            </div>
          </div>
        </div>
        <div>
          <MovieSchedules />
        </div>
      </div>
      <div className="md:hidden pt-20 px-5" id="phone-detail__page">
        <div className="container">
          <div className="flex space-x-4">
            <div className="w-2/5">
              <img className="rounded" src={movie.hinhAnh} alt="" />
            </div>
            <div className="w-3/5 flex flex-col justify-between">
              <div>
                <Progress
                  percent={movie.danhGia * 10}
                  trailColor="yellow"
                  strokeColor={{
                    "0%": "#108ee9",
                    "100%": "#87d068",
                  }}
                  format={(number) => {
                    return (
                      <span className="text-black">{number / 10 + " /10"}</span>
                    );
                  }}
                />
              </div>
              <div>
                <span className="font-bold">{movie.tenPhim}</span>
              </div>
              <div>
                <span>
                  Ngày khởi chiếu:
                  {" " + moment(movie.ngayChieuGioChieu).format("DD-MM-YYYY")}
                </span>
              </div>
              <div>
                <span>
                  Giờ chiếu:
                  {" " + moment(movie.ngayChieuGioChieu).format("HH:MM")}
                </span>
              </div>
              <div>
                <button className="border-2 border-sky-500 block w-full py-2">
                  <a href="#phone-schedules" className="text-sky-500">
                    Chọn xuất chiếu
                  </a>
                </button>
              </div>
            </div>
          </div>
          <div className="my-5">
            <div className="space-y-2">
              <div>
                <span className="uppercase font-bold">Nội dung phim</span>
              </div>
              <div>
                <span>{movie.moTa}</span>
              </div>
            </div>
          </div>
          <div id="phone-schedules">
            <MovieSchedules />
          </div>
        </div>
      </div>
    </div>
  );
}
