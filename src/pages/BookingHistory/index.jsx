import moment from "moment";
import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
// import { useSelector } from "react-redux";
import { userServ } from "services/userService";

const styleBookingPage = {
  backgroundImage:
    "linear-gradient(to top,#000,transparent 70%),url(https://www.maxpixel.net/static/photo/1x/Red-Curtain-Cinema-Background-Theater-Stripes-551797.jpg",
  backgroundSize: "cover",
  backgroundRepeat: "no-repeat",
};

export default function BookingHistory() {
  let [dataRaw, setDataRaw] = useState(null);
  let fetchTicketsHistory = async () => {
    try {
      let res = await userServ.getUserInfor();
      setDataRaw(res.data.content);
    } catch (err) {
      console.log("err: ", err);
    }
  };

  let renderTicketListPurchase = () => {
    if (dataRaw !== null) {
      return dataRaw.thongTinDatVe.map((ticket) => {
        return (
          <div
            key={nanoid()}
            className="df:border-2 df:border-white df:p-2 flex flex-col items-center space-y-4"
          >
            <span className="text-amber-400">
              Ngày giờ đặt:{" "}
              {" " + moment(ticket.ngayDat).format("DD-MM-YYYY - HH:MM")}
            </span>
            <span className="text-amber-400 text-xl">{ticket.tenPhim}</span>
            <span className="text-amber-400">
              Giá vé: {ticket.giaVe + " VNĐ"}
            </span>
            <span className="text-amber-400">
              Thời lượng phim: {ticket.thoiLuongPhim + " phút"}
            </span>
            {ticket.danhSachGhe.map((ghe, index) => {
              return (
                <div key={nanoid()} className="space-x-4">
                  <span className="text-white">Vé {index + 1}:</span>
                  <span className="text-amber-400">{ghe.tenHeThongRap}</span>
                  <span className="text-amber-400">{ghe.tenRap}</span>
                  <span className="text-amber-400">
                    Số ghế:{" " + ghe.tenGhe}
                  </span>
                </div>
              );
            })}
          </div>
        );
      });
    }
  };
  useEffect(() => {
    fetchTicketsHistory();
  }, []);
  return (
    <div
      style={styleBookingPage}
      className="w-screen h-screen text-white font-bold space-y-8 df:px-10 df:py-20 sm:p-32 overflow-y-scroll"
    >
      <div>
        <h1 className="df:text-2xl sm:text-4xl font-bold text-center text-white">
          Lịch sử đặt vé
        </h1>
      </div>
      <div className="grid df:grid-cols-1 df:gap-3 md:grid-cols-2 md:gap-6 xl:grid-cols-3 xl:gap-10  ">
        {renderTicketListPurchase()}
      </div>
    </div>
  );
}
