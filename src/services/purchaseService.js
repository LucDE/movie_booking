import { https } from "./url.config";

export const purchaseServ = {
  getTicketRoomServ: (maLichChieu) => {
    let uri = `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`;
    return https.get(uri);
  },
};
