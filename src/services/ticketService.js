import axios from "axios";
import { userInforLocal } from "./local.service";
import { BASE_URL, TOKEN_CYBERSOFT } from "./url.config";

export const ticketServ = {
  postTicket: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyDatVe/DatVe`,
      method: "POST",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + userInforLocal.get()?.accessToken,
      },
      data,
    });
  },
};
