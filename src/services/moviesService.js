import axios from "axios";
import { userInforLocal } from "./local.service";
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./url.config";

export const moviesServ = {
  getMovieList: () => {
    let uri = `/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`;
    return https.get(uri);
  },
  getDetailMovie: (id) => {
    let uri = `/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`;
    return https.get(uri);
  },
  getMovieScheduleServ: (id) => {
    let uri = `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`;
    return https.get(uri);
  },
  getBannerListServ: () => {
    let uri = "/api/QuanLyPhim/LayDanhSachBanner";
    return https.get(uri);
  },
  getMoviesByTheater: () => {
    let userInfo = userInforLocal.get();
    let uri = `/api/QuanLyRap/LayThongTinLichChieuHeThongRap?pmaNhom=${userInfo?.maNhom}`;
    return https.get(uri);
  },
  deleteMovie: (maPhim) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`,
      method: "delete",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + userInforLocal.get()?.accessToken,
      },
    });
  },
  addMovie: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/ThemPhimUploadHinh`,
      method: "POST",
      headers: {
        "Access-Control-Allow-Origin": "*",
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + userInforLocal.get()?.accessToken,
      },
      data,
    });
  },
  updateMovie: (data) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/CapNhatPhimUpload`,
      method: "POST",
      headers: {
        "Access-Control-Allow-Origin": "*",
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + userInforLocal.get()?.accessToken,
      },
      data,
    });
  },
};
