import { useMediaQuery } from "react-responsive";

export const DesktopReponsive = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 640 });
  return isDesktop ? children : null;
};

export const MobileReponsive = ({ children }) => {
  const isMobile = useMediaQuery({ minWidth: 0, maxWidth: 639.98 });
  return isMobile ? children : null;
};
