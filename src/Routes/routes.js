import Homepage from "pages/HomePage";
import DetailPage from "pages/DetailPage";
import LoginPage from "pages/LoginPage";
import PurchasePage from "pages/PurchasePage";
import SignUpPage from "pages/SignUpPage";
import AccountInfor from "pages/AccountInfor";
import ManagementPage from "pages/ManagementPage";
import SecureView from "HOC/SecureView";
import EditUserPage from "pages/EditUserPage";
import EditFilmPage from "pages/EditFilmPage";
import AddFilmPage from "pages/AddFilmPage";
import AddUserPage from "pages/AddUserPage";

export const routes = [
  {
    path: "/",
    component: <Homepage />,
  },
  {
    path: "/detail/:id",
    component: <DetailPage />,
  },
  {
    path: "/login",
    component: <LoginPage />,
  },
  {
    path: "/sign-up",
    component: <SignUpPage />,
  },
  {
    path: "/purchase/:id",
    component: <PurchasePage />,
  },
  {
    path: "/account-infor",
    component: <AccountInfor />,
  },
  {
    path: "/user-management",
    component: (
      <SecureView>
        <ManagementPage />
      </SecureView>
    ),
  },
  {
    path: "/edit-user/:taiKhoan",
    component: <EditUserPage />,
  },
  {
    path: "/add-user",
    component: <AddUserPage />,
  },
  {
    path: "/edit-film/:maPhim",
    component: <EditFilmPage />,
  },
  {
    path: "/add-film",
    component: <AddFilmPage />,
  },
];
