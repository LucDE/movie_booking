import React from "react";
import { NavLink } from "react-router-dom";

export default function TabMenu() {
  return (
    <div className="df:w-full w-1/2 lg:hidden h-screen flex flex-col space-y-8 p-5">
      <NavLink className=" text-white df:text-xl text-2xl">Trang chủ</NavLink>
      <NavLink className=" text-white df:text-xl text-2xl">
        Phim đang chiếu
      </NavLink>
      <NavLink className=" text-white df:text-xl text-2xl">
        Phim sắp chiếu
      </NavLink>
    </div>
  );
}
