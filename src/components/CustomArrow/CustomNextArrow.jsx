import { RightOutlined } from "@ant-design/icons";
export const NextArrow = ({ currentSlide, slideCount, ...restArrowProps }) => {
  return (
    <div id="carousel-two__arrow">
      <RightOutlined {...restArrowProps} />
    </div>
  );
};
