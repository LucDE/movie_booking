import { LeftOutlined } from "@ant-design/icons";

export const PrevArrow = ({ currentSlide, slideCount, ...restArrowProps }) => {
  return (
    <div id="carousel-two__arrow">
      <LeftOutlined {...restArrowProps} />
    </div>
  );
};
