import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { userInforLocal } from "services/local.service";
import { setUserInfor } from "redux/slice/userSlice";
import swal from "sweetalert";
import TabMenu from "components/TabMenu";
const backGroundStyle = {
  background: "linear-gradient(to right,rgba(0,0,0,1)150px,rgba(0,0,0,.6)100%)",
};

export default function Navbar() {
  let [isMenuOpen, setIsMenuOpen] = useState(false);

  let dispatch = useDispatch();
  let userInfor = useSelector((state) => state.userSlice.userInfor);
  let navigate = useNavigate();

  const handleToggleMenu = () => {
    isMenuOpen = isMenuOpen ? false : true;
    setIsMenuOpen(isMenuOpen);
  };

  const handleReloadPage = () => {
    window.location.href = "/";
  };

  const handleClick = () => {
    navigate("/account-infor");
  };

  const handleLogout = () => {
    swal({
      title: "Bạn có muốn đăng xuất?",
      icon: "info",
      buttons: {
        cancel: "Không",
        confirm: "Đồng ý",
      },
    }).then((value) => {
      if (value) {
        userInforLocal.remove();
        dispatch(setUserInfor(null));
        swal({
          title: "Đã đăng xuất",
          text: "Cảm ơn bạn đã sử dụng ứng dụng",
          icon: "success",
        });
        setTimeout(() => {
          navigate("/");
        }, 2000);
      }
    });
  };
  useEffect(() => {
    let userInfor = userInforLocal.get();
    if (userInfor) {
      dispatch(setUserInfor(userInfor));
    }
  }, []);

  return (
    <div>
      <nav
        style={backGroundStyle}
        className="w-full df:py-1 sm:py-2 xl:py-4 fixed top-0 z-40"
      >
        <div className="flex df:justify-between sm:justify-between md:justify-between lg:justify-around df:px-4 sm:px-4 items-center">
          <div className="flex items-center df:space-x-4 ">
            <div className="bar__button lg:hidden md:block">
              <button onClick={handleToggleMenu}>
                {isMenuOpen ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#fff"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <line x1={18} y1={6} x2={6} y2={18} />
                    <line x1={6} y1={6} x2={18} y2={18} />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#fff"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <line x1={3} y1={12} x2={21} y2={12} />
                    <line x1={3} y1={6} x2={21} y2={6} />
                    <line x1={3} y1={18} x2={21} y2={18} />
                  </svg>
                )}
              </button>
            </div>
            <NavLink
              to="/"
              className="flex items-center"
              onClick={handleReloadPage}
            >
              <span>
                <i className="text-amber-400 text-6xl fab fa-critical-role" />
              </span>
            </NavLink>
          </div>
          <div
            className=" w-full lg:w-auto lg:block hidden"
            id="navbar-default"
          >
            <ul className="flex lg:space-x-10">
              <li>
                <NavLink
                  className=" text-base font-bold uppercase  text-white  hover:text-sky-400  hover:ease-in transition"
                  to="/"
                  onClick={handleReloadPage}
                >
                  Trang chủ
                </NavLink>
              </li>
              <li>
                <a
                  href="#search__bar"
                  className=" text-base font-bold uppercase text-gray-400  hover:text-sky-400  hover:ease-in transition"
                >
                  Phim đang chiếu
                </a>
              </li>
              <li>
                <a
                  href="#comming__movies"
                  className=" text-base font-bold uppercase text-gray-400 hover:text-sky-400  hover:ease-in transition"
                >
                  Phim sắp chiếu
                </a>
              </li>
            </ul>
          </div>
          <div className="md:w-auto">
            {userInfor === null ? (
              <ul className="flex items-center space-x-4">
                <li>
                  <NavLink
                    href="#"
                    className="block df:text-xs sm:text-base font-bold uppercase text-gray-400 hover:text-sky-400  hover:ease-in transition"
                    to="/login"
                  >
                    Đăng nhập
                  </NavLink>
                </li>
                <li>
                  <NavLink
                    to="/sign-up"
                    className="block df:text-xs  sm:text-base font-bold uppercase py-2 pr-4 pl-3 text-gray-400 hover:text-sky-400  hover:ease-in transition"
                  >
                    Đăng ký
                  </NavLink>
                </li>
              </ul>
            ) : (
              <ul className="flex items-center rounded-lg space-x-8">
                <li>
                  <button
                    className="flex justify-center items-center "
                    onClick={handleClick}
                  >
                    <img
                      className="df:w-6 lg:w-8 bg-white rounded-full mr-2"
                      src="https://uxwing.com/wp-content/themes/uxwing/download/peoples-avatars/man-person-icon.png"
                      alt=""
                    />
                    <span className="df:text-xs sm:text-base font-bold uppercase text-gray-400 hover:text-sky-400 hover:ease-in transition">
                      {userInfor.hoTen}
                    </span>
                  </button>
                </li>
                <li>
                  <button
                    href="#"
                    className="space-x-2 flex items-center  font-bold uppercase text-gray-400 hover:text-red-500  hover:ease-in transition"
                    onClick={handleLogout}
                  >
                    <span>
                      <i className="df:text-xl sm:text-2xl fa fa-sign-out-alt"></i>
                    </span>
                    <span className="df:text-xs sm:text-base">Đăng xuất</span>
                  </button>
                </li>
              </ul>
            )}
          </div>
        </div>
        {isMenuOpen && <TabMenu />}
      </nav>
    </div>
  );
}
